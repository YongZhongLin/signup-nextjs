import Head from 'next/head'
import Image from 'next/image'
import { Inter } from '@next/font/google'
import styles from '@/styles/Home.module.css'
import backgroundImage from '../assets/bg.jpg'
import LandingForm from './auth/_landingForm'

const inter = Inter({ subsets: ['latin'] })

export default function Home() {
  return (
    <>
      <Head>
        <title>Create Your Account</title>
        <meta name="description" content="Create Your Account" />
        <meta name="viewport" content="width=device-width, initial-scale=1" />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <main className={styles.main}>
        {/* Background Image */}
        <Image
          className={styles.landingImage}
          src={backgroundImage}
          alt="Landing image"
          layout='fill'
          objectFit='cover'
          objectPosition='center'
        />
        {/* Sign Up Form */}
        <LandingForm />
      </main>
    </>
  )
}
