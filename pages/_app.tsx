import '@/styles/globals.css'
import fontawesome from '@fortawesome/fontawesome'
import { library } from '@fortawesome/fontawesome-svg-core'
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'
import type { AppProps } from 'next/app'

fontawesome.library.add(faEye, faEyeSlash)
export default function App({ Component, pageProps }: AppProps) {
  return <Component {...pageProps} />
}
