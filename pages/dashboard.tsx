import React from 'react'
import Head from 'next/head'
import { useState } from 'react';
import homeStyle from '@/styles/Home.module.css'

function Dashboard() {
    return (
        <>
            <Head>
                <title>Dashboard</title>
                <meta name="description" content="Create Your Account" />
                <meta name="viewport" content="width=device-width, initial-scale=1" />
                <link rel="icon" href="/favicon.ico" />
            </Head>
            <main className={homeStyle.main}>
                    <p>Dashboard</p>
            </main>

        </>

    )
}

export default Dashboard