
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { FC, SetStateAction, useState } from "react";
import styles from "@/styles/Auth.module.css";
import { faEye, faEyeSlash } from '@fortawesome/free-solid-svg-icons'

interface Props {
  password: string;
  setPassword: (value: SetStateAction<string>) => void;
}

export const PasswordField: FC<Props> = ({
  password,
  setPassword,
}): JSX.Element => {
  const [visible, setVisible] = useState<boolean>(false);
  return (
    <>
      {/* Logic */}
      <input
        value={password}
        type={visible ? "text" : "password"}
        className={styles.input_area}
        onChange={(e) => setPassword(e.target.value)}
      />

      <span className={styles.eye_icon}>
        {
          visible ? 
          <FontAwesomeIcon
            icon={faEyeSlash}
            onClick={() => setVisible(!visible)}
          /> :
          <FontAwesomeIcon
            icon={faEye}
            onClick={() => setVisible(!visible)}
          />
        }
      </span>
    </>
  );
};