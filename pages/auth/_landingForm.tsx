import React from 'react'
import { useState } from 'react';
import Image from 'next/image'
import styles from '@/styles/Auth.module.css'
import webLogo from '@/assets/logo.png'
import PasswordStrengthMeter from './components/passwordStrengthMeter'
import { useForm } from "react-hook-form";
import { PasswordField } from './components/passwordField';
import { useRouter } from 'next/router';

function LandingForm() {
  const [password, setPassword] = useState('');
  const [error, setError] = useState(false)
  const router = useRouter();

  const handleSubmit = e => {
    e.preventDefault();
    const requirements = [
      // Must be at least 8 characters
      password.length >= 8,
      // Must contain at least 1 uppercase letter
      /[A-Z]/.test(password),
      // Must contain at least 1 lowercase letter
      /[a-z]/.test(password),
      // Must contain at least 1 number
      /\d/.test(password),
      // Must contain at least 1 special character
      /[ `!@#$%^&*()_+\-=\[\]{};':"\\|,.<>\/?~]/.test(password)
    ]
    const isValid = requirements.every(Boolean)

    if (isValid) {
      // alert('Password is valid!')
      router.push('/dashboard')
    } else {
      setError(true)
    }
  }

  return (
    <div className={styles.landingForm}>
      <div className={styles.center}>
        <Image
          src={webLogo}
          alt="Logo"
        />
      </div>
      <div className={styles.center}>
        <h2>Create Your Account</h2>
      </div>

      <form onSubmit={handleSubmit}>
        <div className={styles.formData}>
          <label className={styles.formLabel} for="name">Full name:</label>
          <input className={styles.input_area} type="text" id="first" name="first" />
        </div>
        <div className={styles.formData}>
          <label className={styles.formLabel} for="email">Email:</label>
          <input className={styles.input_area} type="email" id="email" name="email" />
        </div>
        <div className={styles.formData}>
          <label className={styles.formLabel} for="pwd">Password:</label>
          <PasswordField password={password} setPassword={setPassword} />
          {error && <p>Password is not valid!</p>}
        </div>
        <PasswordStrengthMeter password={password} />
        <button className={styles.btn} type="submit">Continue</button>
      </form>

    </div>
  )
}

export default LandingForm